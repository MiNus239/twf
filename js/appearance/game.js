const game = new PIXI.Application({
    backgroundColor: 0x22303e,
    autoDensity: true,
    resizeTo: window
});

game.stage.sortableChildren = true;

const tileDimensions = window.innerWidth >= 500
    ? Math.floor(window.innerHeight / 32)
    : Math.floor(window.innerWidth / 21);

const scale = tileDimensions / 32;
const movingSpeed = 0.7;

const numOfBorderTilesHeight = Math.floor(game.view.height / tileDimensions);
const numOfBorderTilesWidth = Math.floor(game.view.width / tileDimensions);

const gameHeight = numOfBorderTilesHeight * tileDimensions;
const gameWidth = numOfBorderTilesWidth * tileDimensions;

game.view.height = gameHeight;
game.view.width = gameWidth;

const ruleBlocksWrapper = new PIXI.Container();
ruleBlocksWrapper.sortableChildren = true;

game.stage.addChild(ruleBlocksWrapper);

export { game, tileDimensions, scale, gameWidth, gameHeight, ruleBlocksWrapper, numOfBorderTilesHeight, numOfBorderTilesWidth, movingSpeed }