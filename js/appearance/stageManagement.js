import {game} from "./game.js";
import {renderBlocks} from "./renderGamingBlocks.js";

function makeObjectStageSwitcher(obj, newStage, oldStage, exercise=null) {
    obj.on('pointerup', () => {
        document.body.replaceChild(newStage.view, oldStage.view);
        if (exercise && newStage === game) renderBlocks(exercise);
    });
    obj.on('tap', () => {
        document.body.replaceChild(newStage.view, oldStage.view);
        if (exercise && newStage === game) renderBlocks(exercise);
    });
}

function switchStage(newStage, oldStage) {
    document.body.replaceChild(newStage.view, oldStage.view);
}

export { makeObjectStageSwitcher, switchStage }