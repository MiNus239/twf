import {createTextSprite, mainTextStyle} from "./objects/text";
import {Level} from "./Level";
import {game, gameWidth, movingSpeed, scale, tileDimensions, gameHeight} from "./appearance/game";
import {Timer} from "./objects/Timer";
import {Chain, createChainForBlock, chainAngle} from "./objects/static/Chain";
import {renderBg} from "./appearance/renderBg";
import {generateArrayOfRandomAssets} from "./appearance/renderGamingBlocks";
import {ruleBlocksContainerY} from "./appearance/constantPositions";
import {renderExercise} from "./index";
import {removeSprite} from "./mechanics/helpers";


const textStyle = new PIXI.TextStyle(mainTextStyle);
const numberOfLevelsOnOnePage = 15;
let MenuBlockContainer = null;
const baseMenuBlockMoveSpeedMultiplier = 15;
let pagesBegins = []
let pagesEnds = []
let currentPage = -1;


function MakePagesBorders() {
        let maxBlocksOnStage = gameWidth / tileDimensions;
        let currentBlocks = 0;
        pagesBegins.push(0);
        for (let i = 0; i < Level.levels.length; i++) {
            const exp = createTextSprite(Level.levels[i].name, textStyle);
            const numOfBlocks = 0;
            if (Math.ceil(exp.width / (2 * tileDimensions)) >= 3) {
                numOfBlocks = Math.ceil(exp.width / (2 * tileDimensions));
            }
            else {
                numOfBlocks = 3;
            }
            currentBlocks += (numOfBlocks * 3 - 1);
            if (currentBlocks > maxBlocksOnStage) {
                pagesEnds.push(i);
                pagesBegins.push(i);
                currentBlocks = numOfBlocks - 1;
            }
        }
        pagesEnds.push(Level.levels.length);
}

let container1 = new PIXI.Container();
let container2 = new PIXI.Container();
let text = createTextSprite(``, textStyle);

function addCurrentPage(){
    game.stage.removeChild(text);
    if (currentPage !== -1)
        text = createTextSprite(`Current Page: ${currentPage + 1}/${pagesBegins.length}`, textStyle);
    else
        text = createTextSprite(``, textStyle);
    text.resolution = 2;
    text.x = gameWidth - tileDimensions * 10;
    text.y = tileDimensions * 1.5;
    game.stage.addChild(text);
}

function addArrowsToStage() {
    const rightArrow = new PIXI.Sprite.from('./assets/inca_right_arrow.png');
    const leftArrow = new PIXI.Sprite.from('./assets/inca_left_arrow.png');
    rightArrow.scale.set(2 * scale);
    leftArrow.scale.set(2 * scale);

    container1.x = gameWidth / 2 + 4 * tileDimensions;
    container1.y = 2 * gameHeight / 3;
    container2.x = gameWidth / 2 - 16 * tileDimensions;
    container2.y = 2 * gameHeight / 3;
    container1.addChild(rightArrow);
    container2.addChild(leftArrow);

    const text1 = createTextSprite("Next Page", textStyle);
    text1.anchor.set(0.5);
    const text2 = createTextSprite("Previous Page", textStyle);
    text2.anchor.set(0.5);
    text1.x = 3 * 3.25 * tileDimensions / 2;
    text1.y = 4.25 * tileDimensions;
    text2.x = 3 * 3.75 * tileDimensions / 2;
    text2.y = 4.25 * tileDimensions;

    container1.addChild(rightArrow);
    container2.addChild(leftArrow);
    container1.addChild(text1);
    container2.addChild(text2);

    rightArrow.alpha = 0.8;
    rightArrow.interactive = true;
    rightArrow.buttonMode = true;
    rightArrow.zIndex = 2;
    rightArrow.on("pointerup", () => nextPage());

    leftArrow.alpha = 0.8;
    leftArrow.interactive = true;
    leftArrow.buttonMode = true;
    leftArrow.zIndex = 2;
    leftArrow.on("pointerup", () => previousPage());


    game.stage.addChild(container1);
    game.stage.addChild(container2);
}

function removeArrows(){
    game.stage.removeChild(container1);
    game.stage.removeChild(container2);

}


function createMenuBlock(container, expressionSprite, numOfBlocks, blockStyle, scaleCoefficient, alpha=null) {
    let urls = generateArrayOfRandomAssets(numOfBlocks, blockStyle);
    for (let i = 0; i < urls.length; i++) {
        const sprite = new PIXI.Sprite.from(urls[i]);
        sprite.scale.set(scaleCoefficient * scale);
        sprite.x = i * scaleCoefficient * tileDimensions;
        sprite.y = 0;
        if (alpha) sprite.alpha = alpha;
        container.addChild(sprite);
    }

    container.addChild(expressionSprite);
    expressionSprite.anchor.set(0.5);
    expressionSprite.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    expressionSprite.y = tileDimensions;

    container.pivot.x = numOfBlocks * scaleCoefficient * tileDimensions / 2;
    container.pivot.y = tileDimensions * scaleCoefficient / 2;

    return container;
}

export class MenuBlock {
    static lowered = false;
    static allMenuBlocksArr = [];
    static moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;

    static resetMenuBlock() {
        MenuBlock.lowered = false;
    }
    constructor(name, blockFunction) {
        MenuBlock.lowered = !MenuBlock.lowered;

        this.sprite = new PIXI.Container();
        this.sprite.sortableChildren = true;

        const exp = createTextSprite(name, textStyle);
        const numOfBlocks = Math.ceil(exp.width / (2 * tileDimensions)) >= 3
            ? Math.ceil(exp.width / (2 * tileDimensions))
            : 3;
        this.block = createMenuBlock(
            new PIXI.Container(),
            exp,
            numOfBlocks,
            "front",
            2,
            0.85
        );

        this.block.interactive = true;
        this.block.buttonMode = true;
        this.block.zIndex = 2;
        this.block
            .on("pointerup", blockFunction);

        this.chain = createChainForBlock(MenuBlock.lowered);
        this.block.y += this.chain.height;

        this.sprite.addChild(this.block);
        this.sprite.addChild(this.chain);


        this.intervalFunction = () => {
            this.sprite.x += movingSpeed * MenuBlock.moveSpeedMultiplier;
            this.sprite.y += movingSpeed * MenuBlock.moveSpeedMultiplier * Math.cos(chainAngle);

            if (this.sprite.x > gameWidth + numOfBlocks * tileDimensions * 2) {
                //clearInterval(this.sprite.interval);
                //removeSprite(this.sprite);
            }
        };

        this.sprite.stop = () => clearInterval(this.sprite.interval);

        this.sprite.continue = () => this.sprite.interval = setInterval(this.intervalFunction, 1);

        this.sprite.interval = this.sprite.continue();
        MenuBlock.allMenuBlocksArr.push(this.sprite);
    }
}

function blocksStop(reverse=false) {
    const interval = setInterval(() => {
        if (reverse){
            if (MenuBlock.moveSpeedMultiplier < 0) {
                MenuBlock.moveSpeedMultiplier += 0.5;
            }
            else {
                MenuBlock.moveSpeedMultiplier = 0;
                clearInterval(interval);
            }
        }
        else {
            if (MenuBlock.moveSpeedMultiplier > 0) {
                 MenuBlock.moveSpeedMultiplier -= 0.5;
            }
            else {
                MenuBlock.moveSpeedMultiplier = 0;
                clearInterval(interval);
            }
        }
    }, 1);
}

function blocksGo(reverse=false){
    const interval = setInterval(() => {
            if (!reverse){
                if (MenuBlock.moveSpeedMultiplier < baseMenuBlockMoveSpeedMultiplier) {
                    MenuBlock.moveSpeedMultiplier += 0.5;
                }
                else {
                    MenuBlock.moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;
                    clearInterval(interval);
                }
            }
            else {
                if (MenuBlock.moveSpeedMultiplier > -baseMenuBlockMoveSpeedMultiplier) {
                     MenuBlock.moveSpeedMultiplier -= 0.5;
                }
                else {
                    MenuBlock.moveSpeedMultiplier = -baseMenuBlockMoveSpeedMultiplier;
                    clearInterval(interval);
                }
            }
        }, 1);
}


function addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse, koef=1.2, lower=false) {
    MenuBlock.moveSpeedMultiplier = baseMenuBlockMoveSpeedMultiplier;
    if (reverse === true) {
        MenuBlock.moveSpeedMultiplier = -baseMenuBlockMoveSpeedMultiplier;
        arrayOfButtonsFunctions.reverse();
        arrayOfButtonsNames.reverse();
    }
    MenuBlockContainer = new PIXI.Container();
    MenuBlockContainer.y = ruleBlocksContainerY;
    game.stage.addChild(MenuBlockContainer);
    let i = 0;
    const firstBlock = new MenuBlock(arrayOfButtonsNames[i], arrayOfButtonsFunctions[i]).sprite;
    firstBlock.children[0].y = MenuBlock.lowered
        ? MenuBlockContainer.y + tileDimensions * 5
        : MenuBlockContainer.y + tileDimensions;
    if (reverse ===true) {
        firstBlock.x = gameWidth;
        firstBlock.y = gameWidth * Math.cos(chainAngle);
    }
    MenuBlockContainer.addChild(firstBlock);
    MenuBlockContainer.x -= firstBlock.width / 2;
    MenuBlockContainer.y -= firstBlock.width * Math.cos(chainAngle) / 2;
    i++;
    const interval = setInterval(() => {
        if (i === arrayOfButtonsNames.length){
            const lastChild = MenuBlockContainer.children[MenuBlockContainer.children.length - 1];
            const firstChild = MenuBlockContainer.children[0];
            if ((lastChild.x + firstChild.x >= gameWidth - 15 * tileDimensions && reverse === false) ||
                (lastChild.x + firstChild.x <= gameWidth + 15 * tileDimensions && reverse === true)){
                //MenuBlock.moveSpeedMultiplier = 0;
                blocksStop(reverse);
                //Chain.stop();
                clearInterval(interval);
            }
        }
        else {
            const lastChild = MenuBlockContainer.children[MenuBlockContainer.children.length - 1];
            if ((lastChild.x >= koef * lastChild.width && reverse === false) ||
                (lastChild.x <=  gameWidth - koef * lastChild.width  && reverse === true)) {
                if (lower === false) {
                    MenuBlock.resetMenuBlock();
                }
                const newBlock = new MenuBlock(arrayOfButtonsNames[i], arrayOfButtonsFunctions[i]).sprite;
                if (reverse === false) {
                    newBlock.x -= newBlock.width / 2;
                    newBlock.x += tileDimensions * 2;
                    newBlock.y = newBlock.x * Math.cos(chainAngle);
                }
                else {
                    newBlock.x += gameWidth + newBlock.width / 2;
                    newBlock.x -= tileDimensions * 2;
                    newBlock.y = newBlock.x * Math.cos(chainAngle);
                }
                MenuBlockContainer.addChild(newBlock);
                i++;
            }
        }
    }, 1);
}

function removeAllBlocks(nextAction, reverse=false) {
    blocksGo(reverse);
    const interval = setInterval(() => {
        let lastChild = MenuBlockContainer.children[0];
        MenuBlockContainer.children.forEach(child => {
            if ((child.x > lastChild.x && reverse === true)||
                (child.x < lastChild.x && reverse === false)){
                lastChild = child;
            }
        });
        if ((lastChild.x > gameWidth + lastChild.width) || (lastChild.x < -lastChild.width)){
            //console.log(lastChild.x, gameWidth + lastChild.width);
            //MenuBlockContainer.children.forEach(child => console.log(child.x));
            MenuBlock.allMenuBlocksArr.forEach(x => {removeSprite(x)});
            MenuBlock.allMenuBlocksArr = [];
            removeSprite(MenuBlockContainer);
            MenuBlockContainer = null;
            nextAction();
            clearInterval(interval);
        }
    }, 1);
}

function  addMainMenuBlocks(reverse=false) {
    MenuBlock.resetMenuBlock();
    let arrayOfButtonsNames = ["Exit", "Rules", "Settings", "Choose Level"];
    let arrayOfButtonsFunctions = [
        () => {},
        () => {},
        () => {},
        () => {nextPage(); addArrowsToStage(); addCurrentPage();},
    ];
    if (Level.lastPlayedIndex === 0) {
        arrayOfButtonsNames.push("Start Game");
    }
    else {
        arrayOfButtonsNames.push("Continue");
    }
    arrayOfButtonsFunctions.push(() => startGame(Level.lastPlayedIndex + 1))
    addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse);
}



function nextPage() {
    if (pagesEnds[currentPage] !== Level.levels.length) {
        removeAllBlocks( () => {
            currentPage++;
            addCurrentPage();
            addLevelsBlockToMainMenu(false);
        }, false);
    }
}


function startGame(levelId, reverse) {
    removeAllBlocks(() => {
        Level.currentIndex = levelId - 1;
        renderExercise();
    }, reverse);
}

function previousPage() {
    removeAllBlocks( () => {
        if (pagesBegins[currentPage] !== 0) {
            currentPage--;
            addCurrentPage();
            addLevelsBlockToMainMenu(true);
        }
        else{
            currentPage--;
            addCurrentPage();
            removeArrows();
            addMainMenuBlocks(true);
        }
    }, true);
}

function addLevelsBlockToMainMenu(reverse) {
    //let arrayOfButtonsNames = ["Next Page"];
    //let arrayOfButtonsFunctions = [() => nextPage()];
    let arrayOfButtonsNames = [];
    let arrayOfButtonsFunctions = [];
    for(let i = pagesEnds[currentPage] - 1; i >= pagesBegins[currentPage]; i--){
        arrayOfButtonsNames.push(Level.levels[i].name);
        arrayOfButtonsFunctions.push(() => startGame(i + 1, reverse));
    }
    //arrayOfButtonsNames.push("Previous Page");
    //arrayOfButtonsFunctions.push(() => previousPage());
    addBlocksToMainMenu(arrayOfButtonsNames, arrayOfButtonsFunctions, reverse, 0.5, true);
}


export function addMainMenuToStage() {
    renderBg();
    if (pagesBegins.length === 0) {
        MakePagesBorders();
    }
    //pagesBegins.forEach(x => console.log(x));
    const text = createTextSprite(`Main menu`, textStyle);
    text.resolution = 2;
    text.x = tileDimensions * 5;
    text.y = tileDimensions * 1.5;
    game.stage.addChild(text);
    Timer.clear();
    Chain.addToStage();
    addMainMenuBlocks(false);
}