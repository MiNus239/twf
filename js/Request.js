import {getLogin, getUUID, getHardwareProperties, getSpeedCoef, getTimeMultCoef, getAwardMultCoef} from "./index";
import {Level} from "./Level";
import {Timer} from "./objects/Timer.js";
import {Hearts} from "./objects/Hearts.js";


function sendRequest(page, parametersJson) {
    var body = parametersJson.split("\\").join("\\\\");
//    console.log(body);
    const url = "https://mathhelper.space:8443/" + page;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    //xhr.setRequestHeader("Authorization", authenticateUser("admin", "@dmIn"));

    xhr.onreadystatechange = function() {//Вызывает функцию при смене состояния.
        if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            // Запрос завершен. Здесь можно обрабатывать результат.
        } else {
        }
    };
    xhr.send(body);
}

function bodyStart() {
    var milliseconds = new Date().getTime();
    const level = Level.levels[Level.currentIndex];
    var taskId = level.taskId;
    var taskType = level.scope;
    if (taskType == null || taskType === ""){
        taskType = "trigonometry";
    }
    return "{\"game\":\"incas_js\",\"deviceTs\":\"" + milliseconds.toString() +
        "\",\"hardwareDeviceId\":\"" + getUUID() +
        "\",\"hardwareProperties\":\"" + getHardwareProperties() +
        "\",\"hardwareSpeedCoef\":\"" + getSpeedCoef() +
        "\",\"totalTimeMultCoef\":\"" + getTimeMultCoef() +
        "\",\"totalTimeMS\":\"" + Timer.getInitialTimeMS() +
        "\",\"difficulty\":\"" + level.steps +
        "\",\"minSteps\":\"" + level.steps +
        "\",\"taskId\":\"" + taskId +
        "\",\"leftTimeMS\":\"" + Timer.getTimeMS() +
        "\",\"leftLives\":\"" + Hearts.heartsLeft() +
        "\",\"taskType\":\"" + taskType +
        "\",\"login\":\"" + getLogin() + "\""
}

function bodyEnd() {
    return "}"
}


export {sendRequest, bodyStart, bodyEnd};