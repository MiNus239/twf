import MathOperations from "./MathOperations.js";
import { MutationBlock } from "../objects/Block.js";
import {game, movingSpeed} from "../appearance/game.js";
import {gameHeight, gameWidth, tileDimensions} from "../appearance/game.js";
import {Mechanism} from "../objects/static/Mechanism.js";
import {Steps} from "../objects/Steps.js";
import {showCross} from "../objects/static/Cross.js";
import {Goal} from "../index.js";
import {Hearts} from "../objects/Hearts.js";
import {addAlert} from "../appearance/alertScreen.js";
import {removeParenthesis} from "./helpers.js";
import {createTextSprite} from "../objects/text.js";
import {ExerciseSettings} from "../objects/ExerciseSettings";
import {bodyEnd, bodyStart, sendRequest} from "../Request";
import {RuleBlock} from "../objects/Block";
import {chainAngle} from "../objects/static/Chain.js";

const textStyle = new PIXI.TextStyle({
    fill: 'red',
});

function createText(text, x, y, red=false) {
    let sprite = createTextSprite(text, textStyle);
    sprite.x = x;
    sprite.y = y;
    sprite.style = {
        fontFamily: 'Arial',
        fontSize: gameWidth / 30,
        fontStyle: 'italic',
        fontWeight: 'bold',
    };
    if (red) {
        sprite.style.fill = 'red';
        //sprite.interactive = true;
        //sprite.buttonMode = true;
    }
    return sprite;
}

function fall(block) {
    const falling = setInterval(() => {
        if (block.y >= MutationBlock.y) {
            block.parent.removeChild(block);
            clearInterval(falling);
        }
        block.x -= movingSpeed;
        block.y -= movingSpeed * Math.cos(chainAngle);
        block.y += 10 * tileDimensions / 5;
    }, 10);
}

function makeSingleSubstitution(block, subPlace, place) {
    const mutation = MathOperations.applySubstitutionByPlaceCoordinates(
        MutationBlock.topExpression,
        block.rule.left,
        block.rule.right,
        subPlace.parentStartPosition,
        subPlace.parentEndPosition,
        subPlace.startPosition,
        subPlace.endPosition
    );
    sendRequest("math_game_log",
        bodyStart() +
        ",\"action\":\"place\"" +
        ",\"currRule\":\"" + block.rule.left + " : " + block.rule.right +"\"" +
        ",\"currSelectedPlace\":\"" + place +"\"" +
        ",\"currExpression\":\"" + MutationBlock.topExpression +"\"" +
        ",\"nextExpression\":\"" + mutation +"\"" +
        bodyEnd()
    );
    // place block under the mechanism
    //block.parent.removeChild(block);
    //game.stage.addChild(block);
    //block.y = gameHeight / 2;
    //block.x = gameWidth / 2;
    fall(block);
    new MutationBlock(mutation);
    if (removeParenthesis(mutation) === removeParenthesis(Goal.expression)) {
        //win
        sendRequest("math_game_log",
            bodyStart() +
            ",\"action\":\"win\"" +
            bodyEnd()
        );
        addAlert(true);
    } else {
        // manage steps
        Steps.takeStep();

        if (Steps.isLimitReached()) {
            //loss
            Hearts.deleteHeart();
            Goal.moveUp();
            if (Hearts.heartsLeft() === 0) {
                sendRequest("math_game_log",
                    bodyStart() +
                    ",\"action\":\"lose\"" +
                    bodyEnd()
                );
                addAlert();
            }
        }
    }
}

function errorSubstitution(block) {
    block.x = block.startX;
    block.y = block.startY;

    showCross();
}

let substPlaces = null;
let subSegmentText = "";
let bestPlaces = null;
let textInterval = null;
let choiceTextSprites = null;

function releaseRuleBlock(block, rules) {
    choiceTextSprites = [];
    substPlaces = MathOperations.findSubstitutionPlacesCoordinates(
        MutationBlock.topExpression,
        block.rule.left,
        block.rule.right
    );

    sendRequest("math_game_log",
        bodyStart() +
        ",\"action\":\"rule\"" +
        ",\"currRule\":\"" + block.rule.left + " : " + block.rule.right +"\"" +
        ",\"comment\":\"" + substPlaces.length + "\"" +
        ",\"currExpression\":\"" + MutationBlock.topExpression +"\"" +
        bodyEnd()
    );

    if (substPlaces.length === 0) {
        //errorSubstitution(block);
    }

    //else if (substPlaces.length === 1) {
    //    makeSingleSubstitution(block, substPlaces[0], steps);
    //}

    //else if (substPlaces.length > 1) {
    else {
        // mechanism.sprite.onLoop = () => mechanism.sprite.stop();
        // API bug for -1, -1 positions
        if (substPlaces.length > 1 && (substPlaces[0].startPosition === -1 || substPlaces[1].startPosition === -1)) {
            errorSubstitution(block);
        }
        // put block under the mechanism until choice is made
        //Mechanism.await();
        //block.parent.removeChild(block);
        //game.stage.addChild(block);
        //block.y = gameHeight / 2;
        //block.x = gameWidth / 2;

        const x = 3 * gameWidth / 4;
        const y = gameHeight / 6;
        let bgText = createText(
            MutationBlock.topExpression,
            x, y
        );
        bgText.x -= bgText.width / 2;
        bgText.y -= bgText.height / 2;
        bgText.y += tileDimensions * 2;


        //game.stage.addChild(bgText);
        choiceTextSprites.push(bgText);
        let subSegmentSprite = null;
        block.isFormulaOnStage = true;
        textInterval = setInterval(() => {
            if (choiceTextSprites.length == 2) {
                let spr =  choiceTextSprites[1];
                choiceTextSprites.pop(1);
                //game.stage.removeChild(spr);
            }
            bestPlaces = substPlaces[0];
            substPlaces.forEach((places) => {
                const pos = (block.x + block.parent.x - gameWidth / 10) * MutationBlock.topExpression.length / (9 * gameWidth / 10);
                if (places.startPosition <= pos && pos <= places.endPosition) {
                    if (bestPlaces.startPosition <= pos && pos <= bestPlaces.endPosition) {
                        if (bestPlaces.endPosition - bestPlaces.startPosition > places.endPosition - places.startPosition) {
                            bestPlaces = places;
                        }
                    }
                    else {
                        bestPlaces = places;
                    }
                }
                else {
                    if (!(bestPlaces.startPosition <= pos && pos <= bestPlaces.endPosition)) {
                        let dist = Math.min(Math.abs(places.startPosition - pos), Math.abs(pos - places.endPosition));
                        let bestDist = Math.min(Math.abs(bestPlaces.startPosition - pos), Math.abs(pos - bestPlaces.endPosition));
                        if (dist < bestDist) {
                            bestPlaces = places;
                        }
                    }
                }
            });
            let places = bestPlaces;
            subSegmentSprite = null;
            subSegmentText = "";
            if (places.startPosition === 0) {
                subSegmentText = MutationBlock.topExpression.slice(0, places.endPosition + 1);
                subSegmentSprite = createText(subSegmentText, x, y, true);
                subSegmentSprite.x -= bgText.width / 2;
                subSegmentSprite.y -= bgText.height / 2;
                subSegmentSprite.y += tileDimensions * 2;
                //game.stage.addChild(subSegmentSprite);
            }

            if (places.startPosition > 0) {
                const skipText = MutationBlock.topExpression.slice(0, places.startPosition);
                const skipSprite = createText(skipText, x, y);

                subSegmentText = MutationBlock.topExpression.slice(places.startPosition, places.endPosition + 1);
                subSegmentSprite = createText(
                    subSegmentText,
                    x + skipSprite.width,
                    y,
                    true
                );
                subSegmentSprite.x -= bgText.width / 2;
                subSegmentSprite.y -= bgText.height / 2;
                subSegmentSprite.y += tileDimensions * 2;
                //game.stage.addChild(subSegmentSprite);
            }
            choiceTextSprites.push(subSegmentSprite);
        }, 200);
        //subSegmentSprite.on('click', () => {
        //    makeSingleSubstitution(block, places, subSegmentText);
        //    Mechanism.continue();
        //    choiceTextSprites.forEach(text => game.stage.removeChild(text));
        //});
    }
}

export function removeFormulaSprite(){
    clearInterval(textInterval);
    //choiceTextSprites.forEach((text) => game.stage.removeChild(text));
    console.log("lol");
}

class Dragging
{
    static onDragStart(block, event) {
        if (RuleBlock.isDraggingPossible === true) {
            block.startX = block.x;
            block.startY = block.y;
            block.data = event.data;
            block.dragging = true;
            block.alpha = 0.8;
            block.zIndex = 2;
            block.onDragStartInterval = setInterval(() => {
                 block.x -= movingSpeed;
                 block.y -= movingSpeed * Math.cos(chainAngle);
            }, 10);
            releaseRuleBlock(block, block.rules);
        }
    }

    static onDragMove(block) {
        if (block.dragging === true && RuleBlock.isDraggingPossible === true) {
            const newPosition = block.data.getLocalPosition(block.parent);
            block.x = newPosition.x;
            block.y = newPosition.y;
        }
    }

    static onDragEnd(block, rules, steps) {
        if (RuleBlock.isDraggingPossible === true) {
            clearInterval(textInterval);
            //choiceTextSprites.forEach((text) => game.stage.removeChild(text));
            console.log("kek");
            clearInterval(block.onDragStartInterval);
            block.alpha = 1;
            block.zIndex = 1;
            block.dragging = false;
            if (
                (block.y > 0)
            // &&
            // (block.x > tileDimensions * 8 && block.x < gameWidth / 2 + tileDimensions * 8)
            // TODO fix horizontal position
            ) {
                //releaseRuleBlock(block, rules, steps);
                if (block.cnt === 0) {
                    if (substPlaces.length === 0) {
                        errorSubstitution(block);
                    }
                    else {
                        makeSingleSubstitution(block, bestPlaces, subSegmentText);
                        block.cnt = 1;
                    }
                    block.isFormulaOnStage = false;
                }
            } else {
                block.x = block.startX;
                block.y = block.startY;
                block.rotation = 0;
            }

        }
    }
}

export { Dragging }