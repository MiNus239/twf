class ExerciseSettingsClass
{
    constructor() {
        this.scope = 0;

        this.setScope = (scope) => {
            this.scope = scope;
        };
    }
}

export const ExerciseSettings = new ExerciseSettingsClass();