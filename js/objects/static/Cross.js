import {game, scale, gameWidth, gameHeight} from "../../appearance/game.js";

function showCross() {
    const cross = PIXI.Sprite.from('./assets/cross.png');
    cross.anchor.set(0.5);
    cross.x = gameWidth / 2;
    cross.y = gameHeight / 2;
    cross.scale.set(scale * 2);

    let fade = false;
    setInterval(() => {
        fade ? cross.alpha = 0.5 : cross.alpha = 1;
        fade = !fade;
    }, 500);

    setTimeout(() => {
        game.stage.removeChild(cross);
    }, 3000);

    game.stage.addChild(cross);
}

export { showCross }