import { game, scale, tileDimensions } from "../../appearance/game.js";
import {gameWidth} from "../../appearance/game";

export function addLoginInputToStage(currentLogin) {
    var input = new PIXI.TextInput({
        input: {fontSize: '25px', width: '700px'},
        box: {
            fill: 0xE8E9F3, rounded: 16, stroke: {color: 0xCBCEE0, width: 4}
        }
    });
    input.x = gameWidth / 2 - 350;
    input.y = tileDimensions * 1.25;
    input.placeholder = "Enter your login ...";
    if (currentLogin != null && currentLogin !== ""){
        input.text  = currentLogin;
    }
    game.stage.addChild(input);
    input.focus();
    return input;
}