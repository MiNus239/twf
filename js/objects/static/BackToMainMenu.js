import { game, scale, tileDimensions } from "../../appearance/game.js";
import { renderExercise } from "../../index.js"
import {IsGameEnded} from "../../appearance/alertScreen";
import {bodyEnd, bodyStart, sendRequest} from "../../Request";
import {Level} from "../../Level.js"
import {RuleBlock} from "../Block.js";

export function addBackToMainMenuButtonToStage() {
    const backToMainMenuButton = new PIXI.Sprite.from('./assets/backToMainMenu.png');
    backToMainMenuButton.anchor.set(0.5);
    backToMainMenuButton.scale.set(scale * 0.5);
    backToMainMenuButton.x = tileDimensions * 5;
    backToMainMenuButton.y = tileDimensions * 2;
    game.stage.addChild(backToMainMenuButton);

    backToMainMenuButton.interactive = true;
    backToMainMenuButton.buttonMode = true;

    backToMainMenuButton.on("pointerup", () => {
        if (RuleBlock.isDraggingPossible === true) {
            IsGameEnded.isGameEnded = true;
            Level.currentIndex = "mainMenu";
            renderExercise();
        }
    });
}