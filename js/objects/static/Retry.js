import { game, scale, tileDimensions } from "../../appearance/game.js";
import { renderExercise } from "../../index.js"
import {IsGameEnded} from "../../appearance/alertScreen";
import {bodyEnd, bodyStart, sendRequest} from "../../Request";
import {RuleBlock} from "../Block.js";

export function addRetryButtonToStage() {
    const retryButton = new PIXI.Sprite.from('./assets/retry.png');
    retryButton.anchor.set(0.5);
    retryButton.scale.set(scale * 0.5);
    retryButton.x = tileDimensions * 3.5;
    retryButton.y = tileDimensions * 2;
    game.stage.addChild(retryButton);

    retryButton.interactive = true;
    retryButton.buttonMode = true;

    retryButton.on("pointerup", () => {
        if (RuleBlock.isDraggingPossible === true) {
            IsGameEnded.isGameEnded = true;
            //setTimeout(() => IsGameEnded.isGameEnded = false, 100);
            sendRequest("math_game_log",
                bodyStart() +
                ",\"action\":\"win\"" +
                bodyEnd()
            );
            renderExercise();
        }
    });
}