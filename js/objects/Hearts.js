import {game, gameWidth, tileDimensions, scale} from "../appearance/game.js";

class HeartsClass
{
    constructor() {
        this.renderHearts = (amountOfHearts) => {
            this.sprites = [];

            for (let i = 0; i < amountOfHearts; i++) {
                const heartSprite = new PIXI.Sprite.from('./assets/heart.png');
                heartSprite.scale.set(scale * 2);
                heartSprite.anchor.set(0.5);
                heartSprite.x = gameWidth - tileDimensions * 2 - i * tileDimensions * scale * 2;
                heartSprite.y = tileDimensions * 2;
                this.sprites.push(heartSprite);
                game.stage.addChild(heartSprite);
            }
        };

        this.deleteHeart = () => game.stage.removeChild(Hearts.sprites.pop());

        this.heartsLeft = () => this.sprites.length;
    }
}

export const Hearts = new HeartsClass();