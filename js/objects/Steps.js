class StepsClass
{
    constructor() {
        this.taken = 0;
        this.limit = 0;

        this.setLimit = (limit) => {
            this.limit = limit;
        };

        this.takeStep = () => {
            this.taken++;
        };

        this.isLimitReached =  () => {
            return this.taken >= this.limit;
        };

        this.resetSteps = () => {
            this.taken = 0;
            this.limit = 0;
        }
    }
}

export const Steps = new StepsClass();